# Folders Filter

Permet de parcourir un répertoire et ses sous-répertoires à la recherche de répertoires contenant des fichiers.

La recherche est filtrable:
* le dossier dont le nom
 * commence par ...
 * se termine par ...
 * contient ...
 * ne contient pas ...

On peut définir plusieurs filtres. Ils ont chacun leur nom pour les retrouver dans la liste.

Pour:
* lancer la recherche, sélectionner un filtre et cliquer sur "exécuter".
* ajouter un filtre, cliquer sur "nouveau"
* pour modifier un filtre, le sélectionner dans la liste puis cliquer "modifier".

Voilà, c'est simple et débile, mais ça rend bien du service en bureautique.

Le programme est écrit en Java et fonctionne bien avec Windows et Ubuntu (installer OpenJDK) ; ça devrait aussi fonctionner avec Mac OS.

Pour utiliser le programme, télécharger le fichier FoldersFilter.jar et le déposer quelque part sur son disque dur puis cliquer dessus pour le lancer.
