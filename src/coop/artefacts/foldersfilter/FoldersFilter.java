package coop.artefacts.foldersfilter;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;

import coop.artefacts.foldersfilter.FolderFilter.FilterType;

public class FoldersFilter {

	private JFrame frame;

	JComboBox<FolderFilter> comboBox;
	DefaultListModel<String> matchingFoldersModel;
	JButton filterExecuteButton ;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FoldersFilter window = new FoldersFilter();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FoldersFilter() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 511, 300);
		// center the jframe on screen
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));

		comboBox = new JComboBox<FolderFilter>();
		panel.add(comboBox, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.SOUTH);

		filterExecuteButton = new JButton("Exécuter");
		panel_1.add(filterExecuteButton);

		JSeparator separator = new JSeparator();
		panel_1.add(separator);

		JSeparator separator_1 = new JSeparator();
		panel_1.add(separator_1);

		JButton filterEditButton = new JButton("Modifier");
		panel_1.add(filterEditButton);

		JButton filterAddButton = new JButton("Nouveau");
		panel_1.add(filterAddButton);

		JButton filterDelButton = new JButton("Supprimer");
		panel_1.add(filterDelButton);

		this.matchingFoldersModel = new DefaultListModel<String>();
		final JList<String> matchingFolders = new JList<String>(this.matchingFoldersModel);
		matchingFolders.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		MatchingFolderListCellRenderer mflcr = new MatchingFolderListCellRenderer();
		matchingFolders.setCellRenderer(mflcr);

		matchingFolders.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent event) {

				System.out.println("list click " + event.getSource());

				@SuppressWarnings("unchecked")
				JList<String> list = (JList<String>) event.getSource();

				// Get index of item clicked

				int index = list.locationToIndex(event.getPoint());
				String item = (String) list.getModel().getElementAt(index);
				try {
					Desktop.getDesktop().open(new File(item));
				} catch (IOException e1) {

					e1.printStackTrace();
				}
				// Toggle selected state
				// item.setSelected(!item.isSelected());
				// Repaint cell
				list.repaint(list.getCellBounds(index, index));
			}
		});

		final JScrollPane listScrollPane = new JScrollPane(matchingFolders);
		// frame.getContentPane().add(matchingFolders, BorderLayout.CENTER);
		frame.getContentPane().add(listScrollPane, BorderLayout.CENTER);

		/*
		 * // frame.pack(); // make the frame half the height and width
		 * Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		 * int height = screenSize.height; int width = screenSize.width;
		 * frame.setSize(width/2, height/2);
		 */

		try {
			loadData();
		} catch (BackingStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		filterDelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				FolderFilter filter = (FolderFilter) comboBox.getSelectedItem();
				if (filter == null)
					return;

				if (JOptionPane.showConfirmDialog(new JFrame(), "Supprimer le filtre '" + filter.name + "' ?",
						"Heu ?...", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

					comboBox.removeItem(filter);
					saveData();
				}

			}
		});

		filterAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				FilterEditor d = new FilterEditor();
				d.setFolderFilter(null);

				d.setVisible(true);

				if (d.getFolderFilter() == null)
					return;
				System.err.println(d.getFolderFilter().dump());

				comboBox.addItem(d.getFolderFilter());

				saveData();
			}
		});

		filterEditButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				FilterEditor d = new FilterEditor();

				FolderFilter filter = (FolderFilter) comboBox.getSelectedItem();
				if (filter == null)
					return;
				d.setFolderFilter(filter);

				d.setVisible(true);

				if (d.getFolderFilter() == null)
					return;
				System.err.println(d.getFolderFilter().dump());

				comboBox.removeItem(filter);

				comboBox.addItem(d.getFolderFilter());
				comboBox.setSelectedItem(d.getFolderFilter());

				saveData();
			}
		});

		filterExecuteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				final FolderFilter filter = (FolderFilter) comboBox.getSelectedItem();
				if (filter == null)
					return;

				matchingFoldersModel.clear();
				filterExecuteButton.setEnabled(false);
				
				Thread t = new Thread()
				{
			        public void run()
			        {
						walk( filter, filter.folder );
						filterExecuteButton.setEnabled(true);
			        }
			      };
			      t.start();
			      
			}
		});

	}

	public int walk(FolderFilter filter, String path) {

		File root = new File(path);
		File[] list = root.listFiles();

		int childrenCount = 0;

		if (list == null)
			return childrenCount;

		for (File f : list) {
			if (f.isDirectory()) {

				System.out.println("Dir:" + f.getAbsoluteFile());

				int cc = walk(filter, f.getAbsolutePath());

				if (cc > 0 && filter.match(f.getName())) {
					this.matchingFoldersModel.addElement(f.getAbsolutePath());
				}
			} else {
				childrenCount++;
			}
		}
		return childrenCount;
	}

	private void saveData() {

		Preferences prefs = Preferences.userNodeForPackage(getClass());

		try {
			Preferences filtersNode = prefs.node("filters");
			filtersNode.removeNode();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}

		Preferences filtersNode = prefs.node("filters");

		for (int i = 0; i < this.comboBox.getItemCount(); i++) {
			FolderFilter ff = comboBox.getItemAt(i);
			System.out.println("saving " + ff.name);
			Preferences cn = filtersNode.node(ff.name);
			cn.put("name", ff.name);
			cn.put("folder", ff.folder);
			cn.put("filter", ff.filter);
			cn.put("filterType", ff.filterType.toString());
		}
	}

	private void loadData() throws BackingStoreException {

		Preferences prefs = Preferences.userNodeForPackage(getClass());

		Preferences filtersNode = prefs.node("filters");
		for (String childName : filtersNode.childrenNames()) {
			Preferences filterNode = filtersNode.node(childName);
			FolderFilter f = new FolderFilter();
			f.name = filterNode.get("name", null);
			if (f.name == null)
				continue;
			f.folder = filterNode.get("folder", null);
			f.filter = filterNode.get("filter", null);
			f.filterType = FilterType.fromString(filterNode.get("filterType", null));

			comboBox.addItem(f);
		}
	}

}
