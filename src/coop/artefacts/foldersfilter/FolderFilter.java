package coop.artefacts.foldersfilter;

import java.io.Serializable;
import java.util.Locale;

public class FolderFilter implements Serializable {

	public enum FilterType {

		CONTIENT, NECONTIENTPAS, COMMENCEPAR, TERMINEPAR;

		public static FilterType fromString(String name) {
			return Util.getEnumFromString(FilterType.class, name);
		}
	};

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String name;
	public String folder;
	public String filter;
	public FilterType filterType;

	@Override
	public String toString() {
		return this.name;
	}

	public String dump() {
		return "name:[" + this.name + "], folder:[" + this.folder + "], filter:[" + this.filter + "], filterType:["
				+ this.filterType + "]";
	}

	public boolean match(String path) {

		System.out.println("testing " + path);

		// System.out.println(haystack.matches("(?i)" + Pattern.quote(needle) +
		// ".*"));

		switch (filterType) {
		case CONTIENT:
			return path.toUpperCase(Locale.getDefault()).contains(filter.toUpperCase(Locale.getDefault()));
		case NECONTIENTPAS:
			return !path.toUpperCase(Locale.getDefault()).contains(filter.toUpperCase(Locale.getDefault()));
		case COMMENCEPAR:
			return path.toUpperCase(Locale.getDefault()).startsWith(filter.toUpperCase(Locale.getDefault()));
		case TERMINEPAR:
			return path.toUpperCase(Locale.getDefault()).endsWith(filter.toUpperCase(Locale.getDefault()));
		}
		return false;
	}

}
