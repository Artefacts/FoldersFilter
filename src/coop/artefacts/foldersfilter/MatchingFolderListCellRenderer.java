package coop.artefacts.foldersfilter;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class MatchingFolderListCellRenderer extends JPanel
implements ListCellRenderer<String> {

	String folder ;
    JLabel label = new JLabel();
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList list, String folder, int index, boolean isSelected,
			boolean cellHasFocus) {

		this.folder = folder ;

        this.setLayout( new BorderLayout() );
        //JButton btn = new JButton("ouvrir");
        //this.add( btn, BorderLayout.WEST );
        this.add( label, BorderLayout.CENTER);

        if( isSelected )
        	setOpaque(true);
        else
        	setOpaque(false);
       /* btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					Desktop.getDesktop().open(new File(MatchingFolderListCellRenderer.this.folder ));
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			}
		});*/

        label.setText( folder.toString() );
		return this;
	}

	public MatchingFolderListCellRenderer() {
		
        //setOpaque(true);
        //setHorizontalAlignment(CENTER);
        //setVerticalAlignment(CENTER);

		this.setPreferredSize(new Dimension(200, 25));


    }



}
