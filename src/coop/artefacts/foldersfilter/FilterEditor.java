package coop.artefacts.foldersfilter;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import coop.artefacts.foldersfilter.FolderFilter.FilterType;

public class FilterEditor extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField folderTextField;
	private JTextField filterContentTextField;
	private JTextField filterNametextField;

	JRadioButton rdbtnCommencePar;
	JRadioButton rdbtnTerminePar;
	JRadioButton rdbtnContient;
	JRadioButton rdbtnNeContientPas;

	FolderFilter folderFilter = null;

	public FolderFilter getFolderFilter() {
		return this.folderFilter;
	}

	public void setFolderFilter(FolderFilter folderFilter) {
		this.folderFilter = folderFilter;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			FilterEditor dialog = new FilterEditor();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FilterEditor() {

		setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 529, 300);
		this.setLocationRelativeTo(null);

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.PAGE_AXIS));
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				JLabel lblNewLabel_1 = new JLabel("Nom: ");
				panel.add(lblNewLabel_1);
			}
			{
				filterNametextField = new JTextField();
				filterNametextField.setFont(new Font("Dialog", Font.PLAIN, 14));
				panel.add(filterNametextField);
				filterNametextField.setColumns(16);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			panel.setLayout(new BorderLayout(0, 0));
			{
				JLabel lblNewLabel = new JLabel("Chemin");
				panel.add(lblNewLabel, BorderLayout.WEST);
			}
			{
				folderTextField = new JTextField();
				folderTextField.setEditable(false);
				panel.add(folderTextField, BorderLayout.CENTER);
				folderTextField.setColumns(10);
			}
			{
				JButton btnNewButton = new JButton("Parcourir");
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						final JFileChooser fc = new JFileChooser();
						fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						int returnVal = fc.showOpenDialog(FilterEditor.this);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							File file = fc.getSelectedFile();
							System.err.println(file.getAbsolutePath());
							folderTextField.setText(file.getAbsolutePath());
						}
					}
				});
				panel.add(btnNewButton, BorderLayout.EAST);
			}
		}
		{
			JPanel panel_1 = new JPanel();
			contentPanel.add(panel_1);
			panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
			{
				JPanel panel = new JPanel();
				panel_1.add(panel);
				panel.setLayout(new GridLayout(2, 1, 0, 0));
				{
					rdbtnCommencePar = new JRadioButton("commence par");
					panel.add(rdbtnCommencePar);
				}
				{
					rdbtnTerminePar = new JRadioButton("termine par");
					panel.add(rdbtnTerminePar);
				}
				{
					rdbtnContient = new JRadioButton("Contient");
					rdbtnContient.setSelected(true);
					panel.add(rdbtnContient);
				}
				{
					rdbtnNeContientPas = new JRadioButton("ne contient pas");
					panel.add(rdbtnNeContientPas);
				}
				ButtonGroup group = new ButtonGroup();
				group.add(rdbtnContient);
				group.add(rdbtnNeContientPas);
				group.add(rdbtnCommencePar);
				group.add(rdbtnTerminePar);
			}
			{
				filterContentTextField = new JTextField();
				filterContentTextField.setFont(new Font("Dialog", Font.PLAIN, 14));
				panel_1.add(filterContentTextField);
				filterContentTextField.setColumns(16);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if (filterNametextField.getText().trim().isEmpty()) {
							JOptionPane.showConfirmDialog(null, "Erreur: le filtre doit avoir un nom", "Oups !",
									JOptionPane.CLOSED_OPTION);
							return;
						}
						if (folderTextField.getText().trim().isEmpty()) {
							JOptionPane.showConfirmDialog(null, "Erreur: le filtre doit avoir un chemin", "Oups !",
									JOptionPane.CLOSED_OPTION);
							return;
						}
						if (filterContentTextField.getText().trim().isEmpty()) {
							JOptionPane.showConfirmDialog(null, "Erreur: le filtre doit avoir un contenu", "Oups !",
									JOptionPane.CLOSED_OPTION);
							return;
						}

						FilterEditor.this.folderFilter = new FolderFilter();
						folderFilter.name = filterNametextField.getText().trim();
						folderFilter.folder = folderTextField.getText().trim();
						folderFilter.filter = filterContentTextField.getText().trim();
						if (rdbtnContient.isSelected())
							folderFilter.filterType = FilterType.CONTIENT;
						else if (rdbtnNeContientPas.isSelected())
							folderFilter.filterType = FilterType.NECONTIENTPAS;
						else if (rdbtnCommencePar.isSelected())
							folderFilter.filterType = FilterType.COMMENCEPAR;
						else if (rdbtnTerminePar.isSelected())
							folderFilter.filterType = FilterType.TERMINEPAR;

						FilterEditor.this.setVisible(false);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

		this.addComponentListener(new ComponentListener() {

			public void componentHidden(ComponentEvent e) {
				// System.out.println("dialog hidden");
			}

			public void componentMoved(ComponentEvent e) {
				// System.out.println("dialog moved");
			}

			public void componentResized(ComponentEvent e) {
				// System.out.println("dialog resized");
			}

			public void componentShown(ComponentEvent e) {
				System.out.println("dialog shown");
				if (FilterEditor.this.folderFilter == null)
					return;
				FolderFilter f = FilterEditor.this.folderFilter;
				filterNametextField.setText(f.name);
				folderTextField.setText(f.folder);
				filterContentTextField.setText(f.filter);
				switch (f.filterType) {
				case CONTIENT:
					rdbtnContient.setSelected(true);
					break;
				case NECONTIENTPAS:
					rdbtnNeContientPas.setSelected(true);
					break;
				case COMMENCEPAR:
					rdbtnCommencePar.setSelected(true);
					break;
				case TERMINEPAR:
					rdbtnTerminePar.setSelected(true);
					break;
				}
			}
		});

	}

}
